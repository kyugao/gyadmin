package controllers

import (
	"github.com/astaxie/beego"
	"net/http"
	"fmt"
)

type BaseRestfulController struct {
	beego.Controller
}

func (ctrl *BaseRestfulController) ServeJson(result interface{}) {

	if ctrl.Ctx.Request.Method == http.MethodOptions {
		ctrl.Ctx.ResponseWriter.Status = http.StatusOK
	} else {
		ctrl.Data["json"] = result
		ctrl.ServeJSON()
	}
}

type BaseResp struct {
	Code string `json:"code"`
	Info string `json:"info"`
}

type PageResp struct {
	BaseResp
	IsEnd bool
}

func (resp *BaseResp) SetRC(rc *BaseResp, param ... string) {
	if resp == nil || rc == nil {
		return
	}
	resp.Code = rc.Code
	if len(param) > 0 {
		resp.Info = fmt.Sprintf(rc.Info, param)
	} else {
		resp.Info = rc.Info
	}
}
