package main

import (
	_ "gitlab.com/kyugao/gyadmin/routers"
	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}
